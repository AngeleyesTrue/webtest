var gulp = require('gulp');

var bower = require("gulp-bower");

// js 관련
var unglify = require("gulp-uglify");
var plumber = require("gulp-plumber");
var jshint = require("gulp-jshint");

//  css 관련
var minifyCss = require("gulp-minify-css");
var autoprefixer = require("gulp-autoprefixer");
var sass = require("gulp-ruby-sass");

var browserSync = require("browser-sync").create();
var reload = browserSync.reload;

var config = {
  sassPath: "./src/css",
  bowerDir: "./bower_components"
}

gulp.task('bower', function() {
    return bower()
      .pipe(gulp.dest("dist/lib/"));
});

gulp.task('unglify', function() {
  // place code for your default task here
  gulp.src("src/js/*.js")
    .pipe(unglify())
    .pipe(gulp.dest("dist/js"));
});

gulp.task('error', function() {
    return gulp.src('src/js/*.js')
      .pipe(plumber({
          errorHandler: onError
      }))
      .pipe(gulp.dest('dist/js'));
});

gulp.task('lint', function() {
    return gulp.src("src/js/*.js")
      .pipe(jshint())
      //.pipe(jshint.reporter("default"))
      .pipe(jshint.reporter("jshint-stylish"));
      //.pipe(jshint.reporter("fail"));
});

gulp.task('scripts', function() {
    return gulp.src('src/js/*.js')
      .pipe(plumber({
          errorHandler: onError
      }))
      .pipe(unglify())
      .pipe(gulp.dest('dist/js'));
});

gulp.task('minifyCss', function() {
    gulp.src('src/css/*.css')
        .pipe(minifyCss())
        .pipe(gulp.dest('dist/css'));
});

gulp.task('prefixer', function() {
    gulp.src('src/css/*.css')
        .pipe(autoprefixer({
          browsers: ["last 2 versions"],
          cascade: false
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('sass', function() {
    return sass("src/sass/*.scss")
      .on("error", function(err){
        console.error("Error!", err.message);
      })
      .pipe(gulp.dest("src/css"));
});

gulp.task('css', function() {
    gulp.src('src/css/*.css')
        .pipe(autoprefixer({
          browsers: ["last 2 versions"],
          cascade: false
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('dist/css'));
});

gulp.task('sass-sync', function() {
    return sass("src/sass/*.scss")
      .on("error", function(err){
        console.error("Error!", err.message);
      })
      .pipe(gulp.dest("src/css"))
      .pipe(browserSync.stream({match: "**/*.css"}));
});

gulp.task("watch", function(){
  gulp.watch("src/sass/*.scss", ["sass"]);
});

gulp.task('browser-sync', function() {
    browserSync.init({
      server:{
        baseDir: "./"
      }
    });
    
    gulp.watch("src/sass/*.scss", ["sass-sync"]).on("change", reload);
    gulp.watch("src/js/*.js").on("change", reload);
    gulp.watch("**/*.html").on("change", reload);
});

var onError = function(err){
    console.log(err);
}

gulp.task("default", ["bower", "scripts", "sass", "css"]);